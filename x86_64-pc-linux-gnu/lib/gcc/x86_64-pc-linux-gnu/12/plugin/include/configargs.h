/* Generated automatically. */
static const char configuration_arguments[] = "/workspace/gitpod/x86-builder-gcc/build/gcc/configure --with-pkgversion=xea-xo1-gcc --libdir=/tmp/mostlyportable-gcc/usr/lib --libexecdir=/tmp/mostlyportable-gcc/usr/lib --with-lib-path=/tmp/mostlyportable-gcc/usr/lib --enable-languages=c,c++,lto --with-gcc-major-version-only --prefix=/tmp/mostlyportable-gcc/usr --with-isl=/tmp/mostlyportable-gcc/usr --with-gmp=/tmp/mostlyportable-gcc/usr --with-mpfr=/tmp/mostlyportable-gcc/usr --with-mpc=/tmp/mostlyportable-gcc/usr --with-linker-hash-style=both --with-system-zlib --enable-__cxa_atexit --enable-cet=auto --enable-checking=release --enable-clocale=gnu --enable-default-pie --enable-default-ssp --enable-gnu-indirect-function --enable-gnu-unique-object --enable-linker-build-id --enable-lto --enable-multilib --enable-plugin --enable-shared --enable-threads=posix --disable-libssp --disable-libstdcxx-pch --disable-werror --without-cuda-driver --enable-link-serialization=1 --disable-bootstrap";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "generic" }, { "arch", "x86-64" } };
